@echo off

echo [INFO - %DATE% %TIME%] - The current working location is %cd%

IF [%1] EQU [] (
	echo [ERROR - %DATE% %TIME%] - No input received from User
	echo [ERROR - %DATE% %TIME%] - Please trigger the script with below command
	echo [ERROR - %DATE% %TIME%] - USAGE: %0 path/to/conf.properties
	EXIT /B 1
)

ECHO [INFO - %DATE% %TIME%] - Property file location is %1
ECHO [INFO - %DATE% %TIME%] - Validating the Property File Location

IF NOT EXIST %1 (
	echo [ERROR - %DATE% %TIME%] - Property file does not exists: %1
	echo [ERROR - %DATE% %TIME%] - Aborting the script
	EXIT /B 1
)

ECHO [INFO - %DATE% %TIME%] - Property file is available in the given location

IF [%2] EQU [] (
	echo [WARN - %DATE% %TIME%] - Jar location is not received from User
	echo [WARN - %DATE% %TIME%] - Automatically detecting the jar location from project structure
	
	IF NOT EXIST target\Streaming-1.0-SNAPSHOT.jar (
		echo [ERROR - %DATE% %TIME%] - Unable to read the Jar file from %cd%\target\Streaming-1.0-SNAPSHOT.jar
		echo [ERROR - %DATE% %TIME%] - Aborting the script
		EXIT /B 1
	)
	echo [INFO - %DATE% %TIME%] - The Jar file is available in project structure
)

echo [INFO - %DATE% %TIME%] Invoking Spark-Submit Command with given properties

spark-submit --class com.altimetrik.twitter.streams.spark.driver.TwitterStreamsDriver ^
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.4,com.typesafe:config:1.3.1 ^
--files %1 target\Streaming-1.0-SNAPSHOT.jar

