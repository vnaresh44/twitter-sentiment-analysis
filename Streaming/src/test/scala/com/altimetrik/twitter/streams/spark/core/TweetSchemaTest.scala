package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row

class TweetSchemaTest extends BeforeNAfter {
  test("testTweetSchema"){
    assertResult(List(Row("1236171383677300738")))(dataFrame.select("id").take(1))
    assertResult(List(Row("Sat Jan 07 06:06:39 +0000 2020")))(dataFrame.select("createdAt").take(1))
    assertResult(List(Row("RT @ZeeNews: Depositors' money not at risk, reconstruction draft bill being studied: " +
      "SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K")))(dataFrame.select("text").take(1))
    assertResult(List(Row("en")))(dataFrame.select("lang").take(1))
    assertResult(List(Row("DHAKKAD")))(dataFrame.select("name").take(1))
    assertResult(List(Row("NeerajDhaked")))(dataFrame.select("screenName").take(1))
    assertResult(List(Row("Jaipur, India")))(dataFrame.select("location").take(1))
    assertResult(List(Row("357")))(dataFrame.select("followersCount").take(1))
    assertResult(List(Row("8")))(dataFrame.select("listedCount").take(1))
    assertResult(List(Row("99156")))(dataFrame.select("statusesCount").take(1))
    assertResult(List(Row("0")))(dataFrame.select("retweetCount").take(1))
    assertResult(List(Row("124475")))(dataFrame.select("favoriteCount").take(1))
    assertResult(List(Row("1")))(dataFrame.select("trendId").take(1))
    assertResult(List(Row("key")))(dataFrame.select("keyWord").take(1))
  }
}
