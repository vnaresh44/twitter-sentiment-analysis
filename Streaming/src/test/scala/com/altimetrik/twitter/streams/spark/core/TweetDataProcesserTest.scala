package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

class TweetDataProcesserTest extends BeforeNAfter {

  test("josnDataParser"){
    val data = List(Row("{\"id\":1236230607983665152,\"createdAt\":\"Sat Mar 07 10:01:59 +0000 2020\", " +
      "\"text\":\"RT @Mysterious636: We are trending at no2!!!  Abhi toh trend start bhi nahi hua aur we are " +
      "at no2 trending in India!  This is the power of…\", \"lang\":\"en\", \"userId\":1198356064774713345, " +
      "\"name\":\"Prince SaQib\", \"screenName\":\"PrinceS72105509\", \"location\":\"India\", \"followersCount\":59, " +
      "\"listedCount\":0, \"statusesCount\":13172, \"retweetCount\":0, \"favoriteCount\":3065, \"trendId\":\"1\", " +
      "\"keyWord\":\"key\"}"))
    val testJson = testSparkSession.createDataFrame(testSparkSession.sparkContext.parallelize(data),
      StructType(List(StructField("value", StringType, false))))
    assertResult(List(Row("1236230607983665152","Sat Mar 07 10:01:59 +0000 2020", "RT @Mysterious636: " +
      "We are trending at no2!!!  Abhi toh trend start bhi nahi hua aur we are at no2 trending in India!  " +
      "This is the power of…", "en","1198356064774713345", "Prince SaQib", "PrinceS72105509", "India",
      "59", "0", "13172","0", "3065", "1", "key")))(testTweetDataProcesser.getJsonData(testJson).collect())
  }

  test("dateFormatTest"){
        assertResult(List(Row("07-01-2020 06:06:39 +0000"), Row("07-02-2020 06:06:38 +0000"), Row(
          "07-03-2020 06:06:39 +0000"), Row("07-04-2019 06:06:39 +0000"),
          Row("06-05-2020 06:06:39 +0000"),Row("07-06-2020 06:06:39 +0000"),
          Row("07-07-2020 06:06:39 +0000"), Row("07-08-2020 06:06:39 +0000"),
          Row("07-09-2020 06:06:39 +0000"), Row("07-10-2020 06:06:39 +0000"),
          Row("07-11-2020 06:06:39 +0000"), Row("07-12-2020 06:06:39 +0000"))
        )(testTweetDataProcesser.dateFormat(
          dataFrame).select("createdAt").collect())
  }

  test("removeNullsTest"){
    assertResult(10)(testTweetDataProcesser.removeNulls(dataFrame).count())
  }

//  test("restCallTest"){
//    val restClient:RestClient = new RestClient()
//    val newDf = testSparkSession.readStream.json("")
//    assertResult(new RuntimeException())(testTweetDataProcesser.restCall(newDf, "", restClient))
//  }
}
