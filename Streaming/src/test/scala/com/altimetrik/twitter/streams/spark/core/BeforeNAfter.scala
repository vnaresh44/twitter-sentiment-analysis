package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

class BeforeNAfter extends FunSuite with BeforeAndAfterAll {
  var testTweetDataProcesser: TweetDataProcesser = null
  var testSparkSession: SparkSession = null

  var testData = List(Row("1236171383677300738", "Sat Jan 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
    "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
    "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171377696100353", "Sat Feb 07 06:06:38 +0000 2020", "RT @AdityaMenon22: Media One and Asianet get banned for " +
    "48 hours for reporting on Delhi Violence. But bhakt media can continue with their c…", "en", "83785078",
    "Rangan Chakraborty", "RangC", "Kolkata", "168", "6", "14644", "0", "16244", "1", "Key"),
    Row("1236171382624403457", "Sat Mar 07 06:06:39 +0000 2020", "RT @narendramodi: Here is how we aim to take India " +
    "towards a $5 trillion economy. https://t.co/pu8rRqadJ0", "en", "303363186", "SaveThePlanet", "sonu9691", "New Delhi (India)",
    "48", "0", "1052", "0", "2621", "1", "key"),
    Row("1236171382624403457", "Sat Apr 07 06:06:39 +0000 2019", "", "en", "303363186", "SaveThePlanet", "sonu9691", "New Delhi (India)",
    "48", "0", "1052", "0", "2621", "1", "key"),
    Row("1236171382624403457", "Sat May 06 06:06:39 +0000 2020", "null", "en", "303363186", "SaveThePlanet", "sonu9691", "New Delhi (India)",
    "48", "0", "1052", "0", "2621", "1", "key"),
    Row("1236171383677300738", "Sat Jun 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
    "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
    "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171383677300738", "Sat Jul 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171383677300738", "Sat Aug 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
      Row("1236171383677300738", "Sat Sep 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171383677300738", "Sat Oct 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171383677300738", "Sat Nov 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"),
    Row("1236171383677300738", "Sat Dec 07 06:06:39 +0000 2020", "RT @ZeeNews: Depositors' " +
      "money not at risk, reconstruction draft bill being studied: SBI Chairman on Yes Bank  https://t.co/Z03P0wNZ3K",
      "en", "3065018696", "DHAKKAD", "NeerajDhaked", "Jaipur, India", "357", "8", "99156", "0", "124475", "1", "key"))

  val schema = List(StructField("id", StringType, false), StructField("createdAt", StringType, false),
    StructField("text", StringType, false), StructField("lang", StringType, false), StructField("userId", StringType, false),
    StructField("name", StringType, false), StructField("screenName", StringType, false), StructField("location", StringType, false),
    StructField("followersCount", StringType, false), StructField("listedCount", StringType, false),
    StructField("statusesCount", StringType, false), StructField("retweetCount", StringType, false),
    StructField("favoriteCount", StringType, false), StructField("trendId", StringType, false), StructField("keyWord", StringType, false))
  var dataFrame: DataFrame = null

  override def beforeAll() {
    import org.apache.spark
    testSparkSession = SparkSession.builder().master("local").getOrCreate()
    testTweetDataProcesser = new TweetDataProcesser()
    dataFrame = testSparkSession.createDataFrame(testSparkSession.sparkContext.parallelize(testData), StructType(schema))
  }

  override def afterAll() {
    testSparkSession.close()
    testData = null
    dataFrame = null
    testTweetDataProcesser = null
  }
}

