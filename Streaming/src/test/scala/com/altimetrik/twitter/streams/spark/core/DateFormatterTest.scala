package com.altimetrik.twitter.streams.spark.core
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col

class DateFormatterTest extends BeforeNAfter {
  test("dateFormatTest"){
    assertResult(List(Row("07-01-2020 06:06:39 +0000"), Row("07-02-2020 06:06:38 +0000"),
     Row("07-03-2020 06:06:39 +0000"), Row("07-04-2019 06:06:39 +0000"),
      Row("06-05-2020 06:06:39 +0000"), Row("07-06-2020 06:06:39 +0000"),
      Row("07-07-2020 06:06:39 +0000"), Row("07-08-2020 06:06:39 +0000"),
      Row("07-09-2020 06:06:39 +0000"), Row("07-10-2020 06:06:39 +0000"),
      Row("07-11-2020 06:06:39 +0000"), Row("07-12-2020 06:06:39 +0000"))
    )(dataFrame.select(DateFormatter.date_format(col(
      "createdAt"))).collect())
  }
}
