package com.altimetrik.twitter.streams.spark.driver

import com.altimetrik.twitter.streams.spark.core.{KafkaClient, RestClient, TweetDataProcesser}
import com.altimetrik.twitter.streams.spark.utility.{DataUtils, Utils}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.streaming.Trigger

object TwitterStreamsDriver extends Utils {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.ERROR)
    val tweetDataProcesser: TweetDataProcesser = new TweetDataProcesser()
    val restClient: RestClient = new RestClient()
    val kafkaDataStreams = KafkaClient.getKafkaClient(spark, serverName, serverHost, topicName)

    //val temp = kafkaDataStreams.writeStream.format("console").outputMode("append").start()

    val jsonDf = kafkaDataStreams.selectExpr("CAST(value AS STRING)")

    //val temp = jsonDf.writeStream.format("console").outputMode("append").start()

    val personDF = tweetDataProcesser.getJsonData(jsonDf)

    val nullFilteredDF = tweetDataProcesser.removeNulls(personDF)

    val dateFormattedDF = tweetDataProcesser.dateFormat(nullFilteredDF)

    val query = DataUtils.restCall(dateFormattedDF, restEndPointURL, restClient).trigger(Trigger.ProcessingTime("1 second")).option("startingOffsets", "latest").start()

    val raw = DataUtils.persistData(kafkaDataStreams, rawPath, rawCheckpointDir).start()

    val output = DataUtils.persistData(dateFormattedDF, outputPath, outputPathCheckpointDir).start()

    //temp.awaitTermination()
    raw.awaitTermination()
    output.awaitTermination()
    query.awaitTermination()
  }

}
