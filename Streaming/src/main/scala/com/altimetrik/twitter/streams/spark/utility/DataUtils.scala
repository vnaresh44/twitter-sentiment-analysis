package com.altimetrik.twitter.streams.spark.utility

import com.altimetrik.twitter.streams.spark.core.RestClient
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.streaming.DataStreamWriter

object DataUtils {
  def restCall(dataFrame: DataFrame, url: String, restClient: RestClient): DataStreamWriter[Row] = {
    dataFrame.writeStream.foreachBatch((dataSet, batchId) => {
      dataSet.toJSON.foreachPartition(rows => {
        rows.foreach(row => {
          val returnCode: Int = restClient.postTweet(url, row)//restClient.sendPOST(url, row)
          if (returnCode != 200){
            throw new RuntimeException("Unable to connect to the " + url)
          }
        })
      }
      )
    })
  }

  def persistData(dataFrame: DataFrame, path: String, checkpointDirectory: String, numPartitions: Int = 1,
                  fileFormat: String = "parquet"): DataStreamWriter[Row] = {
    dataFrame.coalesce(numPartitions).writeStream
      .format(fileFormat)
      .option("path", path)
      .option("checkpointLocation", checkpointDirectory)
  }
}
