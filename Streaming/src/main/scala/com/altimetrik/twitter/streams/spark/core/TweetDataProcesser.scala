package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, from_json}

class TweetDataProcesser {
  def removeNulls(dataFrame: DataFrame): DataFrame = {
    dataFrame.where(dataFrame("text") =!="").where(dataFrame("text") =!= "null")
  }

  def getJsonData(dataFrame: DataFrame): DataFrame = {
    dataFrame.select(from_json(dataFrame("value"), TweetSchema.tweetSchema())
        .as("data"))
      .select("data.*")
  }

  def dateFormat(dataFrame: DataFrame): DataFrame = {
    dataFrame.withColumn("createdAt", DateFormatter.date_format(col("createdAt")))
  }
}
