package com.altimetrik.twitter.streams.spark.core

import java.io.OutputStream
import java.net.{HttpURLConnection, MalformedURLException, URL}


class RestClient extends Serializable {

  def postTweet(restUrl: String, tweet: String): Int = {
    var clientConnection: HttpURLConnection = null
    try{
      val url: URL = new URL(restUrl)
      clientConnection = url.openConnection.asInstanceOf[HttpURLConnection]
      clientConnection.setRequestMethod("POST")
      clientConnection.setRequestProperty("Content-Type", "application/json; utf-8")
      clientConnection.setRequestProperty("Accept", "application/json")
      clientConnection.setDoOutput(true)

      val os: OutputStream = clientConnection.getOutputStream
      os.write(tweet.getBytes("utf-8"), 0, tweet.length)

//      if (clientConnection.getResponseCode != 200) {
//        throw new RuntimeException("Unable to connect to the " + restUrl)
//      }
      clientConnection.getResponseCode
    }
//      catch {
//      case e: MalformedURLException => throw new MalformedURLException("Malformed URL received as input " + restUrl + "\n" + e.printStackTrace())
//      case ee: Exception => throw new Exception("Unknown Exception while connecting to the " + restUrl + "\n" + ee.printStackTrace())
//    }
      finally {
      clientConnection.disconnect()
    }
  }


  def sendPOST(restURL:String,jsonString:String):Unit= {
    try {
      val url = new URL(restURL) //your url i.e fetch data from .
      val conn = url.openConnection.asInstanceOf[HttpURLConnection]
      conn.setRequestMethod("POST")
      conn.setRequestProperty("Content-Type", "application/json; utf-8")
      conn.setRequestProperty("Accept", "application/json")
      conn.setDoOutput(true)

      val os = conn.getOutputStream
      try {
        val input = jsonString.getBytes("utf-8")
        os.write(input, 0, input.length)
      } finally if (os != null) os.close()

      if (conn.getResponseCode != 200) throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode)
      conn.disconnect()

    } catch {

      case e: MalformedURLException =>
        System.out.println("Exception in Client:- " + e.printStackTrace())
        throw new MalformedURLException("Malformed URL received as input " + restURL)
      case ee: Exception =>
        println("Unexpected Exception while connecting to client " + ee.printStackTrace())
        throw new Exception("Unexpected Exception raised")
    }
  }
}

