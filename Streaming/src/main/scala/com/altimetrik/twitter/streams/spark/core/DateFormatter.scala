package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.sql.functions.udf

object DateFormatter {

  val date_format = udf(( d : String) => {
    var finalD = ""
    val splitD = d.split(" ")
    val day_num = splitD(2)
    val month = splitD(1) match {
      case "Jan" => "01"
      case "Feb" => "02"
      case "Mar" => "03"
      case "Apr" => "04"
      case "May" => "05"
      case "Jun" => "06"
      case "Jul" => "07"
      case "Aug" => "08"
      case "Sep" => "09"
      case "Oct" => "10"
      case "Nov" => "11"
      case "Dec" => "12"
    }
    val year = splitD(splitD.length-1)
    finalD += day_num+"-"+month+ "-" + year + " " + splitD(3) + " " + splitD(4)
    finalD
  })
}
