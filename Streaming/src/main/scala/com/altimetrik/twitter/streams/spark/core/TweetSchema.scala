package com.altimetrik.twitter.streams.spark.core
import org.apache.spark.sql.types.{DataTypes, StructType}

object TweetSchema {

  def tweetSchema(): StructType = {
    new StructType()
      .add("id",DataTypes.StringType)
      .add("createdAt",DataTypes.StringType)
      .add("text",DataTypes.StringType)
      .add("lang",DataTypes.StringType)
      .add("userId",DataTypes.StringType)
      .add("name",DataTypes.StringType)
      .add("screenName",DataTypes.StringType)
      .add("location",DataTypes.StringType)
      .add("followersCount",DataTypes.StringType)
      .add("listedCount",DataTypes.StringType)
      .add("statusesCount",DataTypes.StringType)
      .add("retweetCount",DataTypes.StringType)
      .add("favoriteCount",DataTypes.StringType)
      .add("trendId",DataTypes.StringType)
      .add("keyWord",DataTypes.StringType)
  }
}
