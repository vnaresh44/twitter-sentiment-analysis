package com.altimetrik.twitter.streams.spark.core

import org.apache.spark.sql.{DataFrame, SparkSession}

object KafkaClient {
  def getKafkaClient(spark: SparkSession, serverName: String, serverHost: String, topicName: String, failOverDataLoss: Boolean = false): DataFrame ={
    spark.readStream
      .format("kafka")
      .option("failOnDataLoss",failOverDataLoss)
      .option(serverName, serverHost)
      .option("subscribe", topicName)
      .load()
  }
}
