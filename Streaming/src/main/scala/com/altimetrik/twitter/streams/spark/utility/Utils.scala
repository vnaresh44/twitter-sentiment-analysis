package com.altimetrik.twitter.streams.spark.utility

import java.io.File

import com.typesafe.config.ConfigFactory
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkFiles
import org.apache.spark.sql.SparkSession

class Utils {

  val spark = SparkSession.builder()
    .appName("Streaming")
    .master("local[*]")
    .getOrCreate()
  //val configFile = SparkFiles.get("config.properties")
  //val config = ConfigFactory.load(ConfigFactory.parseFile(new File(configFile)))
  val config = ConfigFactory.load("config.properties")
  val serverName = config.getString("serverName")
  val serverHost = config.getString("serverHost")
  val topicName = config.getString("topicName")
  val sleepTimer = config.getString("sleepTimer")
  Logger.getLogger("org").setLevel(Level.ERROR)
  val rawPath = config.getString("rawPath")
  val rawCheckpointDir = config.getString("rawCheckpointDir")
  val outputPath = config.getString("outputPath")
  val outputPathCheckpointDir = config.getString("outputPathCheckpointDir")

  val restEndPointURL = config.getString("restEndPointURL")
}

