windows system:

Prerequisites:
Python3.x and pip

#Install the virtual environment package
pip install virtualenv

#Create the virtual environment
python -m venv twitter-poc-venv

#Activate the virtual environment
twitter-poc-venv\Scripts\activate.bat

#Now enter into the project folder
cd django-app\twitter

#Now install the requirements
pip install -r requirements.txt

