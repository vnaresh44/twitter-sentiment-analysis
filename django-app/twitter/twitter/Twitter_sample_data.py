try:
    import sys
    import os
    import logging
    import tweepy as tw
    import pandas as pd
    from datetime import datetime
    from dateutil import parser
    import csv
except ImportError as e:
    print("Module is not importing with error {}".format(str(e)))

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

logger.info("######passing the twitter access keys######")
consumerKey = "WnACC44R71UN7HnmhWCpyrDLf"
consumerSecret = "SUuCdmSEM2nLx4bsGAWDXWGHnT7bSYbZnD0DTPnFGkW9dNm4XF"
access_token = "1149560340817907713-NZV0S5scNyghrTEgdobXMRR3LMOhix"
tokenSecret = "rzDLRCbRORNap1YuuVKvg2UqbMvm1P6jSO5W5DBQ0tvwQ"
##Entering the search keywords####
# search_words = str(input("Enter the search keys: "))
# start_date = parser.parse(input("Enter start date: "))
# end_date = parser.parse(input("Enter end date: "))
#file_name = str(input("Enter the output file name: "))

search_words = ''
tweet_count = 0
start_date = None
end_date = None

if len(sys.argv )==5:
    search_words = sys.argv[1]
    start_date = parser.parse(sys.argv[2])
    end_date = parser.parse(sys.argv[3])
    file_name=sys.argv[4]
elif len(sys.argv) ==4:
    search_words = sys.argv[1]
    start_date = parser.parse(sys.argv[2])
    file_name = sys.argv[3]
else:
    search_words = sys.argv[1]
    tweet_count = int(sys.argv[2])
    file_name = sys.argv[3]
try:
    auth = tw.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(access_token, tokenSecret)
    api = tw.API(auth)
except auth.oauth as e:
    logger.info("Error: Authentication Failed with the Error {}".format(str(e)))
if start_date and end_date:
    print("within start_date and end_date loop")
    tweets = tw.Cursor(api.search,
                  q=search_words+"-filter:retweets",
                  lang="en",since=start_date,
                  until=end_date,exclude_replies=True).items()#Enter number of records for sampling
elif start_date:
    print("within start_date loop")
    tweets = tw.Cursor(api.search,
                       q=search_words + "-filter:retweets",
                       lang="en", since=start_date,
                       exclude_replies=True).items()  # Enter number of records for sampling
else:
    tweets = tw.Cursor(api.search,
                       q=search_words + "-filter:retweets",
                       lang="en",
                       exclude_replies=True).items(tweet_count)  # Enter number of records for sampling

if not tweets:
    logger.error("No tweets are found with the search keys: {}".format(search_words))
else:
    with open(file_name+".csv", 'w') as csvfile: #Creating a new csv
        fieldnames = ['Tweet Text','created_at','Location','name','verified','description','followers_count','Tweet Date','id','statuses_count','created_at','followers_count']
        #Initalizing col names to csv
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        #Writing the header sto csv
        writer.writeheader()
        for tweet in tweets:# Running loop for writing tweets into dict
            if (not tweet.retweeted) and ('RT @' not in tweet.text):
                TweetDic = dict()  #creating the dictionary
                TweetDic['Tweet Text'] = (tweet.text).encode('utf-8')
                TweetDic['created_at'] = (tweet.created_at)
                TweetDic['Location'] = (tweet.author.location).encode('utf-8')
                TweetDic['name'] = (tweet.user.name).encode('utf-8')
                TweetDic['name'] = (tweet.user.verified)
                TweetDic['description'] = (tweet.user.description).encode('utf-8')
                TweetDic['followers_count'] = (tweet.user.followers_count)
                TweetDic['Tweet Date'] = tweet._json['created_at']
                TweetDic['id'] = (tweet.user.id)
                TweetDic['statuses_count'] = (tweet.user.statuses_count)
                TweetDic['created_at'] = (tweet.user.created_at)
                TweetDic['followers_count'] = (tweet.user.followers_count)
                writer.writerow(TweetDic)
                print("working")