import pandas as pd
import nltk
import re
from textblob import TextBlob
import datetime
from datetime import datetime
import matplotlib.pyplot as plt
import emoji


# from wordcloud import WordCloud
# import matplotlib.pyplot as plt
# import numpy as np
# import string
# import seaborn as sns
# import sys
# from nltk.stem.porter import PorterStemmer
# from nltk.corpus import stopwords
# from nltk.tokenize import word_tokenize
# First time required
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('wordnet')


class SentimentAnalyzer:

    def __init__(self):
        path = ""

    def sentiment_scores(self, *args):

        d = dict(*args)
        raw_text = d['text']
        data = pd.DataFrame([d])
        # converting Lowercase
        data['text'] = data['text'].str.lower()

        # Preprocessing
        l1 = []
        for phrase in data.text:
            # Normalising the text
            phrase = re.sub(r"won't", "will not", phrase)
            phrase = re.sub(r"can\'t", "can not", phrase)
            phrase = re.sub(r"n\'t", " not", phrase)
            phrase = re.sub(r"\'re", " are", phrase)
            phrase = re.sub(r"\'s", " is", phrase)
            phrase = re.sub(r"\'d", " would", phrase)
            phrase = re.sub(r"\'ll", " will", phrase)
            phrase = re.sub(r"\'t", " not", phrase)
            phrase = re.sub(r"\'ve", " have", phrase)
            phrase = re.sub(r"\'m", " am", phrase)

            # Removing RT in the Tweet
            phrase = re.sub(r"^rt", "", phrase)

            # Converting Emojis into its real meaning
            phrase = emoji.demojize(phrase)

            # Removing Web(http) links because it wont give any meaning
            phrase = re.sub("(http\S+)", " ", phrase)

            # Removing user name in the tweet
            phrase = re.sub(r'{}.*?{}'.format(re.escape('@'), re.escape(':')), '', phrase, count=1)

            # Removing decimal number in the tweet
            phrase = re.sub("(\S*\d\S*)", " ", phrase)

            # Removing all special Characters
            phrase = re.sub("([^A-Za-z0-9]+)", " ", phrase)

            # Removing extra spaces in the text
            phrase = re.sub("(' +', ' ')", " ", phrase)

            # Removing leading and tailing spaces
            phrase = phrase.strip()
            l1.append(phrase)

        data["text"] = l1

        data["text"] = data.text.apply(lambda x: re.sub("(http\S+)|(\S*\d\S*)|([^A-Za-z0-9]+)", " ", x))

        # tokenization, Lemmatization
        w_tokenizer = nltk.tokenize.WhitespaceTokenizer()
        lemmatizer = nltk.stem.WordNetLemmatizer()

        def lemmatize_text(text):
            return [lemmatizer.lemmatize(w) for w in w_tokenizer.tokenize(text)]

        data['text'] = data.text.apply(lemmatize_text)

        # removing list inside list
        data['text'] = data['text'].apply(lambda x: ' '.join([word for word in x]))

        # Removing stop words
        stop_words = ['few', 'can', 'were', 'between', 'its', 'while', 'all', 'above', 'why', 'on', 'through', 'aren',
                      'from', 'my', 'o', 'other', "she's", 'when', 'don', 'couldn', 'if', "you'd", 'having', 'to',
                      'our', 'in', 'wasn', "it's", 'ain', 'needn', 'mustn', 'or', 'here', 'both', 'do', 'those', 'them',
                      'does', 'shouldn', 'it', 'theirs', 'for', 'that', 'a', 'after', 'further', 're', 'as', 'each',
                      'before', 'same', 'off', 'so', "should've", 'isn', 'hers', 'y', 'had', 'haven', 'he', 'm', 've',
                      "you've", 'being', 's', 'their', "you'll", 'an', 'themselves', 'under', 'been', 'him', 'we', 'd',
                      'and', 'own', 'i', 'than', 'down', 't', "that'll", 'is', 'you', "shan't", 'did', 'hasn', 'this',
                      'into', 'her', 'below', 'any', 'at', 'only', 'be', 'once', 'was', 'are', 'most', 'these', 'didn',
                      'such', 'whom', 'wouldn', 'doing', 'more', 'me', 'until', 'about', 'how', 'then', 'too', 'itself',
                      'of', 'very', 'the', 'weren', 'ma', 'because', 'with', 'they', 'out', 'again', 'by', 'ourselves',
                      'myself', 'doesn', 'won', 'there', 'herself', 'what', 'his', 'where', 'himself', 'will', 'yours',
                      'she', 'shan', 'just', 'over', 'up', 'am', 'yourself', 'which', "you're", 'll', 'nor', 'some',
                      'has', 'during', 'now', 'mightn', 'ours', 'who', 'your', 'have', 'yourselves', "hadn't", 'hadn',
                      'should', 'but']

        data['text'] = data['text'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_words)]))

        # textblob
        polarity = []
        for phrase in data.text:
            analysis = TextBlob(phrase)
            polarity.append(analysis.sentiment.polarity)
            print("Polarity: ", analysis.sentiment.polarity)

        # finding polarity

        p1 = []
        n1 = []
        nu1 = []

        for p in polarity:
            if p > (0.2):
                p1.append(1)
            else:
                p1.append(0)

        for p in polarity:
            if p < (-0.2):
                n1.append(1)
            else:
                n1.append(0)

        for p in polarity:
            if (p > (-0.2) and p < (0.2)):
                nu1.append(1)
            else:
                nu1.append(0)

        l = p1 + n1 + nu1

        if len(d) == 1:
            prediction = {
                'pos': l[0],
                'neg': l[1],
                'neu': l[2],
                'compound': 0
            }
            return prediction

        trend_stream = {
            'trend_id': int(d["trendId"]),
            'tweet_id': int(d["id"]),
            'raw_tweet': raw_text,
            'processed_tweet': data.iloc[0]['text'],
            'location': d['location'],
            'language': d['lang'],
            'favourite_count':d['favoriteCount'],
            'retweets':d['retweetCount'],
            'source':'twitter',
            'positive': l[0],
            'negative': l[1],
            'neutral': l[2],
            'statuses_count': d['statusesCount'],
            'tweeted_date': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            'update_date_time': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            'create_date_time': datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        }

        return trend_stream
