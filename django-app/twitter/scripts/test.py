import pandas as pd
import nltk
import sys
import re
from textblob import TextBlob

from nltk.tokenize import word_tokenize
#Required for the first time
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('wordnet')


def sentiment_analysis(data):
    l1 = list(data["text"])
    data = pd.DataFrame(list(l1), columns=['text'])

    # converting Lowercase
    data['text'] = data['text'].str.lower()

    # Preprocessing
    l1 = []
    for phrase in data.text:
        # specific
        phrase = re.sub(r"won't", "will not", phrase)
        phrase = re.sub(r"can\'t", "can not", phrase)

        # general
        phrase = re.sub(r"n\'t", " not", phrase)
        phrase = re.sub(r"\'re", " are", phrase)
        phrase = re.sub(r"\'s", " is", phrase)
        phrase = re.sub(r"\'d", " would", phrase)
        phrase = re.sub(r"\'ll", " will", phrase)
        phrase = re.sub(r"\'t", " not", phrase)
        phrase = re.sub(r"\'ve", " have", phrase)
        phrase = re.sub(r"\'m", " am", phrase)
        phrase = re.sub(r"rt", "", phrase)
        l1.append(phrase)

    data["text"] = l1

    data["text"] = data.text.apply(lambda x: re.sub("(http\S+)|(\S*\d\S*)|([^A-Za-z0-9]+)", " ", x))

    # tokenization, Lemmatization
    w_tokenizer = nltk.tokenize.WhitespaceTokenizer()
    lemmatizer = nltk.stem.WordNetLemmatizer()

    def lemmatize_text(text):
        return [lemmatizer.lemmatize(w) for w in w_tokenizer.tokenize(text)]

    data['text'] = data.text.apply(lemmatize_text)

    # removing list inside list
    data['text'] = data['text'].apply(lambda x: ' '.join([word for word in x]))

    # Removing stop words
    stop_words = ['few', 'can', 'were', 'between', 'its', 'while', 'all', 'above', 'why', 'on', 'through', 'aren',
                  'from', 'my', 'o', 'other', "she's", 'when', 'don', 'couldn', 'if', "you'd", 'having', 'to', 'our',
                  'in', 'wasn', "it's", 'ain', 'needn', 'mustn', 'or', 'here', 'both', 'do', 'those', 'them', 'does',
                  'shouldn', 'it', 'theirs', 'for', 'that', 'a', 'after', 'further', 're', 'as', 'each', 'before',
                  'same', 'off', 'so', "should've", 'isn', 'hers', 'y', 'had', 'haven', 'he', 'm', 've', "you've",
                  'being', 's', 'their', "you'll", 'an', 'themselves', 'under', 'been', 'him', 'we', 'd', 'and', 'own',
                  'i', 'than', 'down', 't', "that'll", 'is', 'you', "shan't", 'did', 'hasn', 'this', 'into', 'her',
                  'below', 'any', 'at', 'only', 'be', 'once', 'was', 'are', 'most', 'these', 'didn', 'such', 'whom',
                  'wouldn', 'doing', 'more', 'me', 'until', 'about', 'how', 'then', 'too', 'itself', 'of', 'very',
                  'the', 'weren', 'ma', 'because', 'with', 'they', 'out', 'again', 'by', 'ourselves', 'myself', 'doesn',
                  'won', 'there', 'herself', 'what', 'his', 'where', 'himself', 'will', 'yours', 'she', 'shan', 'just',
                  'over', 'up', 'am', 'yourself', 'which', "you're", 'll', 'nor', 'some', 'has', 'during', 'now',
                  'mightn', 'ours', 'who', 'your', 'have', 'yourselves', "hadn't", 'hadn', 'should', 'but']

    data['text'] = data['text'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_words)]))

    # textblob
    polarity = []
    for phrase in data.text:
        analysis = TextBlob(phrase)
        polarity.append(analysis.sentiment.polarity)

    # finding polarity

    p1 = []
    n1 = []
    nu1 = []

    for p in polarity:
        if p > (0.3):
            p1.append(1)
        else:
            p1.append(0)

    for p in polarity:
        if p < (-0.3):
            n1.append(1)
        else:
            n1.append(0)

    for p in polarity:
        if (p > (-0.3) and p < (0.3)):
            nu1.append(1)
        else:
            nu1.append(0)

    # Creating data frame
    # t1 = data.text
    sentiment = pd.DataFrame(list(zip(p1, n1, nu1)), columns=['Positive', 'Negative', 'Neutral'])
    print(sentiment.head(20))




if __name__ == "__main__":
    data = {"text":"i am feeling happy"}
    #sentiment_analysis(['worst'])
    sentiment_analysis(data)