from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from datetime import datetime

class VaderSentimentAnalyzer:

    def __init__(self):
        path = ""

    def sentiment_scores(self, data):

        def binaryScores(x):
            if x > 0.3:
                return [1,0,0]
            elif x < 0.3:
                return [0,1,0]
            else:
                return [0,0,1]

        try:
            score = SentimentIntensityAnalyzer().polarity_scores(data["text"])
        except:
            print("key with text is not found in the sent request")

        if len(data) == 1:
            return score
        else:
            try:
                binaryScore = binaryScores(score["compound"])
                temp = {
                    'positive': binaryScore[0],
                    'negative': binaryScore[1],
                    'neutral': binaryScore[2],
                    'tweet_id': data["id"],
                    'trend_id': data["trendId"],
                    'update_date_time': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                    'create_date_time': datetime.today().strftime('%Y-%m-%d %H:%M:%S')
                }
                return temp
            except:
                raise

