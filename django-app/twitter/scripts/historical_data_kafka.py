import sys
import json
import logging
import tweepy as tw
from dateutil import parser
from datetime import datetime
from pykafka import KafkaClient
from django.conf import settings as env

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def get_kafka_client(use_rdkafka=False):
    client = KafkaClient(hosts=env.KAFKA_HOST)
    topic = client.topics[env.TOPIC]
    logger.info('creating kafka client...')
    return topic.get_producer(use_rdkafka=use_rdkafka)


def json_parser(json_data, search_words, trend_id):
    data = json.loads(json_data)
    country = ''
    if data["place"] != None:
        country = data["place"]["country"]

    return "{" + '"id":' + str(data["id"]) + ',"createdAt":"' + str(data["created_at"]) + '", "text":"' + str(data[
                                                                                                                  "text"]).replace(
        "\n", " ") + '", "lang":"' + str(data["lang"]) + '", "userId":' + str(
        data["user"]["id"]) + ', "name":"' + str(data["user"]["name"]) + '", "screenName":"' + str(
        data["user"]["screen_name"]) + '", "location":"' + str(data["user"]["location"]) + '", "followersCount":' + str(
        data["user"]["followers_count"]) + ', "listedCount":' + str(
        data["user"]["listed_count"]) + ', "statusesCount":' + str(
        data["user"]["statuses_count"]) + ', "retweetCount":' + str(
        data["retweet_count"]) + ', "favoriteCount":' + str(
        data["user"]["favourites_count"]) + ', "country":"' + country + '", "keyWord":"' + search_words + '", "trendId":'+ str(trend_id) + '}'


# Entering the search keywords####
def historical_data(data):
    """ingest historical data in to kakfa"""

    logger.info("received arguments: " + str(data))
    if len(data) < 2:
        logger.error("Please provide at least two arguments")
        logger.error("input should be of format {\"search-word\": \"india\",\"tweet_count\": 100} or {"
                     "\"search-word\": \"india\",\"start_date\": \"19-02-2020\",\"end_date\": \"20-02-2020\"}")
        # sys.exit(-1)
        raise Exception("input should be of format {\"search-word\": \"india\",\"tweet_count\": 100} or {"
                        "\"search-word\": \"india\",\"start_date\": \"19-02-2020\",\"end_date\": \"20-02-2020\"}")

    search_words = ''
    tweet_count = ''
    start_date = None
    end_date = None
    trend_id = 1

    # try:
    if len(data) == 4:
        search_words = data['search_words']
        start_date = parser.parse(data['start_date'])
        end_date = parser.parse(data['end_date'])
        trend_id = data["trendId"]
    else:
        search_words = data['search_words']
        tweet_count = int(data['tweet_count'])
        trend_id = data["trendId"]
    # except Exception as e:
    #   raise
    # api = ''
    auth = None
    # try:
    logger.info("authenticating the user...")
    auth = tw.OAuthHandler(env.CONSUMER_KEY, env.CONSUMER_SECRET)
    auth.set_access_token(env.ACCESS_TOKEN, env.TOKEN_SECRET)
    api = tw.API(auth)
    print(env.CONSUMER_KEY)
    logger.info("authentication succeeded")
    # except auth.oauth as e:
    #   logger.error("Error: Authentication Failed with the Error {}".format(str(e)))
    #  raise
    # since=start_date,until=end_date
    tweets = ''
    # try:
    if start_date and end_date is not None:
        tweets = tw.Cursor(api.search,
                           q=search_words + "-filter:retweets",
                           lang="en", since=start_date, until=end_date, exclude_replies=True).items()
    else:
        tweets = tw.Cursor(api.search,
                           q=search_words + "-filter:retweets",
                           lang="en", exclude_replies=True).items(tweet_count)  # Enter number of records for sampling
    producer = get_kafka_client()
    if not tweets:
        logger.error("No tweets are found with the search keys: {}".format(search_words))
    for tweet in tweets:
        json_obj = json.dumps(tweet._json)
        print(json_parser(json_obj, search_words, trend_id).encode("utf-8"))
        producer.produce(json_parser(json_obj, search_words,trend_id).encode("utf-8"))
        logger.info("tweets are send successfully")
    producer.stop()
    # except Exception as e:
    #   raise
