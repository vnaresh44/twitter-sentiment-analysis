from rest_framework import serializers
from .models import TrendsStream


class TrendsStreamSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrendsStream
        fields = '__all__'

        # fields = ["tweet_id", "trend_id", "positive", "negative", "neutral", "raw_tweet", "processed_tweet", "location",
        #           "language", "favourite_count", "retweets",
        #           "statuses_count", "source", "tweeted_date", "update_date_time", "create_date_time"]


class TwitterTrendsSerializer(serializers.Serializer):
    email = serializers.EmailField()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()
