from django.db import models

# Create your models here.
class TrendsStream(models.Model):
    tweet_id = models.BigIntegerField(primary_key=True)
    trend_id = models.BigIntegerField()
    positive = models.IntegerField()
    negative = models.IntegerField()
    neutral = models.IntegerField()
    raw_tweet = models.CharField(max_length=280)
    processed_tweet = models.CharField(max_length=280)
    location = models.CharField(max_length=200)
    language = models.CharField(max_length=20)
    favourite_count = models.IntegerField()
    retweets = models.IntegerField()
    statuses_count = models.IntegerField()
    source = models.CharField(max_length=32)
    tweeted_date = models.CharField(max_length=128)
    update_date_time = models.CharField(max_length=128)
    create_date_time = models.CharField(max_length=128)

    class Meta:
        db_table = "trends_stream"

    def __str__(self):
        #return str(self.tweet_id)
        return str({
                    "tweet_id":self.tweet_id ,
                    "trend_id": self.trend_id,
                    "raw_tweet":self.raw_tweet,
                    "processed_tweet":self.processed_tweet
                   })


