from django.urls import path

from . import views

app_name = 'nlp'

urlpatterns = [
    path('predict', views.PredictView.as_view(), name='predict'),
    path('word-cloud', views.WordCloud.as_view(), name='wordcloud'),
    path('kafka-historical-data', views.HistoricalData.as_view(), name='historical-data'),
]