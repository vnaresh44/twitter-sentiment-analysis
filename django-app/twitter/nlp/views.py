from rest_framework import views, status
from rest_framework.response import Response
import json
from scripts.textblob_analyzer import SentimentAnalyzer
from scripts.vader_analyzer import VaderSentimentAnalyzer
from .models import TrendsStream  # , Tweets
from rest_framework.permissions import IsAuthenticated
from scripts import historical_data_kafka, wordcloud
import collections
import itertools
import operator

# Create your views here.
from .serializers import TrendsStreamSerializer


class PredictView(views.APIView):
    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            ip = eval(json.dumps(request.data))
            if len(ip) != 1:
                prediction = SentimentAnalyzer.sentiment_scores(self, ip)
                print("*********prediction*********")
                print(prediction)
                # prediction = VaderSentimentAnalyzer.sentiment_scores(self, ip)
                self.saveTweetPrediction(prediction)
                # self.saveTweet(tweet)

            else:
                # prediction = VaderSentimentAnalyzer.sentiment_scores(self, ip)
                prediction = SentimentAnalyzer.sentiment_scores(self, ip)

            return Response(prediction, status=status.HTTP_200_OK, )
        except Exception as e:
            print(repr(e))
            return Response('Erorr Couldn\'t Predict !' + '\n' + repr(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR, )

    def saveTweetPrediction(self, prediction):
        try:
            tweet = TrendsStream(**prediction)
            tweet.save()
        except Exception as e:
            print(repr(e))
            raise

    def saveTweet(self, tweet):
        try:
            tweet = Tweets(**tweet)
            tweet.save()
        except Exception as e:
            print(repr(e))
            raise


class HistoricalData(views.APIView):

    def post(self, request):
        # data = json.dumps(request)
        try:
            historical_data_kafka.historical_data(eval(json.dumps(request.data)))
            return Response('Success', status=status.HTTP_200_OK, )
        except Exception as e:
            print(repr(e))
            return Response('Error Occurred ' + repr(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR, )


class WordCloud(views.APIView):

    def get(self, request):

        try:
            trend_id = request.query_params['trend_id']
        except:
            return Response("'trend_id' query parameter is required", status=status.HTTP_500_INTERNAL_SERVER_ERROR, )

        try:
            records = TrendsStream.objects.all().filter(trend_id=trend_id)
            records_with_all_fields = TrendsStreamSerializer(records, many=True).data
            to_cal = ""
            for e in records_with_all_fields:
                to_cal += e['processed_tweet']

            word_count = {}
            for token in to_cal.split():
                if token in word_count.keys():
                    word_count[token] += 1
                else:
                    word_count[token] = 1
            word_count_sorted = sorted(word_count.items(), key=operator.itemgetter(1), reverse=True)
            converted_to_dict = collections.OrderedDict(word_count_sorted)
            n_iterator = itertools.islice(converted_to_dict.items(), 0, 10)
            elements = []
            for i in range(10):
                try:
                    x = next(n_iterator)
                    print(x[0])
                    d = {}
                    d['text'] = x[0]
                    d['weight'] = x[1]
                    elements.append(d)
                except Exception as e:
                    break
                # elements.append(x)
                # elements.append(next(n_iterator))
            return Response(elements)
        except Exception as e:
            return Response('Error Occurred while processing the tweets ' + repr(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR, )


class CurrentTrendScores(views.APIView):

    def get(self, request):
        result = TrendsStream.objects.raw('SELECT avg(positive) FROM twitter_trends')
        return result
