from requests import Response
from rest_framework import viewsets
from . import models, serializers
from .models import TwitterTrends


class WordCloudViewSet(viewsets):

    model = TwitterTrends

    def list(self, request):
        queryset  = TwitterTrends.objects.raw('SELECT * FROM twitter_trends')
        serializer = MyModelSerializer(queryset, many=True)
        return Response(serializer.data)
