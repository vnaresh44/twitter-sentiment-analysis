from django.test import TestCase
from nlp.models import TwitterTrends#, Tweets
import random
from datetime import datetime


# Create your tests here.
class ModelsTests(TestCase):
    def test_twitter_trends_str(self):
        """Test the Twitter Trends model string representation"""
        tweet_prediction = TwitterTrends.objects.create(
            positive=0,
            negative=0,
            neutral=1,
            tweet_id=random.randint(0, 100000000),
            trend_id=1,
            update_date_time=datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
            create_date_time=datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        )
        self.assertEqual(str(tweet_prediction), str(tweet_prediction.tweet_id))

    def test_tweets_str(self):
        """Test the Twitter Trends model string representation"""
        tweet_info = Tweets.objects.create(
            tweet_id=random.randint(0, 100000000),
            trend_id=1,
            raw_tweet='good http://',
            processed_tweet='Good',
            location='India',
            language='English',
            tweeted_date=datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        )
        self.assertEqual(str(tweet_info), str(tweet_info.tweet_id))
