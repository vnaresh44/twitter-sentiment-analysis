from django.contrib import admin
from .models import TrendsStream


# Register your models here.

@admin.register(TrendsStream)
class TwitterTrendsAdmin(admin.ModelAdmin):
    list_display = ('tweet_id', 'trend_id', 'negative', 'neutral', 'positive')

    list_filter = ('negative', 'neutral', 'positive')

# admin.site.register(TwitterTrends,TwitterTrendsAdmin)
